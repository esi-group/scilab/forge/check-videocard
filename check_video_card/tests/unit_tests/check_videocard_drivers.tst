// ====================================================================
// Allan CORNET
// Copyright DIGITEO 2010
// ====================================================================
root_path = getenv('TOOLBOX_CHECK_VIDEOCARD','');
if  root_path <> '' then
  exec(root_path+'loader.sce');
end
// ====================================================================
// 2K_XP|ATI|DEFAULT|0 0 0 0|http://ati.amd.com/support/driver.html
// non determinated version
os = "2K_XP";
manufacturer = "ATI";
model = "DEFAULT";
drivers_version = "0 0 0 0";
ref = %f;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// 2K_XP|INTEL|DEFAULT|6 14 10 5009|
os = "2K_XP";
manufacturer = "INTEL";
model = "DEFAULT";
drivers_version = "6 14 10 5009";
ref = %t;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// 2K_XP|INTEL|DEFAULT|6 14 10 5009|
os = "2K_XP";
manufacturer = "INTEL";
model = "DEFAULT";
drivers_version = "6 14 10 5008";
ref = %f;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// 2K_XP|INTEL|DEFAULT|6 14 10 5009|
os = "2K_XP";
manufacturer = "INTEL";
model = "DEFAULT";
drivers_version = "6 14 10 5011";
ref = %t;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// SEVEN|NVIDIA|DEFAULT|8 15 11 8631|http://www.nvidia.com/content/drivers/drivers.asp
os = "SEVEN";
manufacturer = "NVIDIA";
model = "DEFAULT";
drivers_version = "8 15 11 8631";
ref = %t;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// SEVEN|NVIDIA|NVIDIA GeForce 8800 GTS|8 16 11 9107|http://www.nvidia.com/content/drivers/drivers.asp
os = "SEVEN";
manufacturer = "NVIDIA";
model = "NVIDIA GeForce 8800 GTS";
drivers_version = "8 16 11 9107";
ref = %t;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// SEVEN|NVIDIA|NVIDIA Quadro NVS 290|8 16 11 9166|http://www.nvidia.com/content/drivers/drivers.asp
os = "SEVEN";
manufacturer = "NVIDIA";
model = "NVIDIA Quadro NVS 290";
drivers_version = "8 16 11 9166";
ref = %t;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================
// SEVEN|NVIDIA|NVIDIA GeForce 9300M GS|8 15 11 8631|http://www.nvidia.com/content/drivers/drivers.asp
os = "SEVEN";
manufacturer = "NVIDIA";
model = "NVIDIA GeForce 9300M GS";
drivers_version = "8 15 11 8631";
ref = %t;
r = check_videocard_drivers(os, manufacturer, model, drivers_version);
if r <> ref then pause,end;
// ====================================================================

