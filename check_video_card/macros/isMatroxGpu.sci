// Copyright 2010 - DIGITEO - Allan CORNET
//

function b = isMatroxGpu()
  b = %f;
  vc = getVideoCard();
  if grep(vc,'/\bMATROX/i','r') <> [] then
    b = %t;
  end
endfunction
