// Copyright 2010 - DIGITEO - Allan CORNET
//

function b = isAtiGpu()
  b = %f;
  vc = getVideoCard();
  if grep(vc,'/\bATI/i','r') <> [] then
    b = %t;
  else 
    if grep(vc,'/\bRADEON/i','r') <> [] then
      b = %t;
    end
  end

endfunction
