// Copyright 2010 - DIGITEO - Allan CORNET
//

function vc = getVideoCard()
  vc = '';
  if MSDOS then
    info = getdebuginfo();
    msg = strsplit(_("Video card: %s"),':');
    p = grep(info, msg);
    line_model = info(p(1));
    model_vc = strsplit(line_model, ':');
    vc = stripblanks(model_vc(2));
    if vc == 'ERROR' then
      vc = '';
    end 
  end
endfunction
